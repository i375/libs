# libs
index of my libraries

- [**SRM**: Scope based resource management library for C](https://bitbucket.org/i375/srm)
- [**GL MATH**: Portable GL Math, C library, based on gl-matrix JavaScript WebGL matrix library](https://bitbucket.org/i375/glmath_library)
- [**SFB**: System independent frame buffer](https://bitbucket.org/i375/sfb_library)
